<?php

namespace Drupal\kgl_evaluation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EvaluationForm.
 */
class EvaluationForm extends FormBase {

  /**
   * Form unique ID.
   *
   * @return string
   *   Form ID.
   */
  public function getFormId() {
    return 'evaluation_form';
  }

  /**
   * Form builder.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Enter valid name here.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validate form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('name');
    if (empty(trim($name))) {
      $form_state->setErrorByName('name', $this->t('Inserted name is invalid.'));
    }
    if (strlen(trim($name)) > 0 && !preg_match("/^([a-zA-Z' ]+)$/", $name)) {
      $form_state->setErrorByName('name', $this->t('Inserted name contains invalid characters. Only letters are allowed!'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Submit form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Add message in logger and display form success message.
    $name = $form_state->getValue('name');
    $this->logger('kgl_evaluation')->notice($this->t('Form successfully submitted with value: @name.', [
      '@name' => $name,
    ]));
    $this->messenger()->addMessage($this->t('Your submission is successful , Submitted name: @name', [
        '@name' => $name,
    ]));
  }

}
